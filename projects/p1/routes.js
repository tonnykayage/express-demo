var TodoController = require('./controllers/TodoController')

module.exports = function(router){
    var router = router;

    router.get('/', function (req, res) {
        //TODO: Server status
    })

    router.post('/create', function(req, res){
        TodoController.create(req, res);
    })

    router.get('/get/:id', function(req, res){
        TodoController.get(req, res);
    })

    router.post('/update', function(req, res){
        TodoController.update(req, res);
    })

    router.delete('/destroy/:id', function(req, res){
        TodoController.delete(req, res);
    })

    router.get('/index', function(req, res){
        TodoController.index(req, res);
    })

    router.post('/auto', function(req, res){
        TodoController.auto(req, res);
    })
    return router;
};