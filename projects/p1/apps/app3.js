var express = require('express');
var app = express()
var bodyParser = require('body-parser');

var routes = require('../routes')(express.Router())
var dbConfig = require('../config')

app.use(bodyParser.json());
app.use('/', routes);
dbConfig();

app.listen(3003, () => {
    console.log("App3 server started")
})